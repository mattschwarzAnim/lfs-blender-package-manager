# Les Fées Spéciales update add-on for Blender

This Blender add-on fetches, installs and updates scripts and
dependencies.

## Note
This branch is the one we use internally for our current project. If you’re interested in
using or developping this program, please head over to the
[public branch](https://gitlab.com/lfs.coop/blender/lfs-blender-package-manager/-/tree/public)
and base any merge request on it.

We’ll have the `master` branch point to it after the end of the project, not to break
anything for our project.

## Installing
Download this add-on in zip format, and install it from Blender’s User
Preferences.

## Usage
Go to the add-on’s user preferences, search for its name (_LFS Package
Manager_), and after activating it, unfold its preferences by clicking
the arrow to the left of the name.

A list of packages appears, with a button to install or update the
package, as well as a button to install them all at once.

## Configuration
Have a look at the config.json in the add-on directory.

``` json
{
    "install_path": "",
    "package_list_url": "https://gitlab.com/lfs.coop/blender/lfs-blender-package-manager/-/raw/master/packages.json"
}
```
`install_path` allows you to choose a custom path for script
installation. When left empty, the path will be automatically chosen
by Blender according to the platform.

`package_list_url` points to a json file on the Internet, which
contains a dictionary of packages to install and associated settings.
This allows you, for instance, to specify a different set of add-ons
for different projects.

### Package options ###

The packages json file, pointed to by the latter option, contains these fields:

``` json
[
    {
        "name": "Camera plane",
        "platform": ["Linux", "Windows", "Darwin"],
        "service": "gitlab",
        "group": "lfs.coop/blender",
        "project": "camera-plane",
        "ref_type": "branch",
        "ref": "master",
        "activation": "camera-plane",
        "categories": ["Layout"],
        "files": {"addons": ["*.py"]}
    },
    {
    ...
    }
]
```

- `name`: A name for this add-on. Should generally be the same as the
  actual add-on, but is only used in the GUI.
- `platform`: Optional list of platform names, for add-ons available
  only on specific OSs. The names are those returned by Python’s
  [`platform.system()`](https://docs.python.org/3/library/platform.html#platform.system).
- `service`: The website hosting the add-on. Currently supported
  services are gitlab and github.
- `group`: The URL subpath to the project. On GitHub, this would be a
  user or organisation. On GitLab, additional group and subgroup may
  be used.
- `project`: The name of the project on the forge.
- `ref_type`: Type of git ref to use for download. May be `branch` or `commit`.
- `ref`: The git ref to download.
  - If `ref_type` is `branch`, the add-on will query the latest commit
    in this branch from the forges’s API, and get that commit.
  - If `ref_type` is `commit`, it will be directly downloaded.
- `activation`: The add-on name, either `<addon>.py` or `<addon>/`
  directory. This is used to show an activation checkbox next to the
  add-on’s name in the list.
- `categories`: A way to group add-ons by category in the UI. If the
  list is empty, the add-on will go into a nameless category.
- `files`: A dictionary of destinations in the Blender script
  directory, and for each, a list of files to copy there. Paths should
  be full zip paths, so a glob may be used to specify them more easily.

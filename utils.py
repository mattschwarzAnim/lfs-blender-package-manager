# LFS Package Manager, copyright (C) 2020-2021 Les Fées Spéciales
# voeu@les-fees-speciales.coop
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.


import bpy
import requests
import os
import json
from .logger import logger
from collections.abc import MutableMapping


TIMEOUT = (2.0, 5.0)

config_path = os.path.join(os.path.dirname(__file__), "config.json")


def get_install_path():
    '''Get destination directory, where add-ons are stored'''
    addon_prefs = bpy.context.preferences.addons[__package__].preferences
    install_path = ''

    if addon_prefs is None:
        install_path = bpy.utils.user_resource("SCRIPTS")
    elif addon_prefs.destination == 'CONFIG':
        install_path = config['install_path']
    elif addon_prefs.destination == 'PREFERENCES':
        install_path = bpy.context.preferences.filepaths.script_directory
    elif addon_prefs.destination == 'USER':
        install_path = bpy.utils.user_resource("SCRIPTS")
    if not install_path:
        install_path = bpy.utils.user_resource("SCRIPTS")
    install_path = os.path.abspath(os.path.expanduser(install_path))
    return install_path


def get_local_info_path():
    '''Get path where the list of downloaded files is stored'''
    addon_prefs = bpy.context.preferences.addons[__name__.split('.')[0]].preferences
    info_dir = ''

    if addon_prefs is None:
        info_dir = os.path.dirname(__file__)
    elif addon_prefs.destination == 'CONFIG':
        info_dir = config['install_path']
    elif addon_prefs.destination == 'PREFERENCES':
        info_dir = bpy.context.preferences.filepaths.script_directory
    elif addon_prefs.destination == 'USER':
        info_dir = os.path.dirname(__file__)
    if not info_dir:
        info_dir = os.path.dirname(__file__)
    info_dir = os.path.abspath(os.path.expanduser(info_dir))
    return info_dir


def get_request(url):
    try:
        with requests.get(url, timeout=TIMEOUT, stream=True) as response:
            content = response.content
    except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as e:
        logger.error("Could not connect to server " + url)
        logger.error("%s" % e)
    else:
        return content


class Config(dict):
    def __init__(self):
        self.read_config()

    def read_config(self):
        """Get config from disk"""
        if os.path.isfile(config_path):
            with open(config_path, 'r') as f:
                self.update(json.load(f))

    def update_config(self):
        """Write config to disk"""
        with open(config_path, 'w') as f:
            json.dump(self, f, indent=4)

config = Config()


class LocalInfo(dict):
    """A dictionary that reads and writes a JSON file to disk"""
    def read_local_info(self):
        info_dir = get_local_info_path()
        if "latest.json" in os.listdir(info_dir):
            local_info_filepath = os.path.join(info_dir, "latest.json")
            with open(local_info_filepath, 'r') as f:
                try:
                    local_info = json.load(f)
                except json.decoder.JSONDecodeError:
                    local_info = {}
        else:
            local_info = {}

        self.update(local_info)

    def write_local_info(self):
        info_dir = get_local_info_path()

        with open(os.path.join(info_dir, "latest.json"), 'w') as f:
            json.dump(self, f, indent=4)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.read_local_info()

    def __setitem__(self, key, value):
        super().__setitem__(key, value)
        self.write_local_info()

    def __delitem__(self, key):
        super().__delitem__(key)
        self.write_local_info()

local_info = LocalInfo()
